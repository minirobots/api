"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import logging
import tornado.ioloop
from minirobots.sqlalchemy import session_factory

from minirobots import options
from minirobots.routes import routes


logging.basicConfig(
    level=options.log_level,
    datefmt='%Y-%m-%d %H:%M:%S',
    format='[%(levelname)s %(asctime)s.%(msecs)d %(module)s:%(lineno)d] %(message)s')


class Application(tornado.web.Application):
    def __init__(self):
        settings = {
            'debug'          : options.debug,
            'login_url'      : '/login',
            'gzip'           : True,
            'session_factory': session_factory(options.db_path, pool_size=20, pool_pre_ping=True),
            'google_oauth'   : {
                'key'   : options.google_client_id,
                'secret': options.google_client_secret,
            },
        }
        super(Application, self).__init__(routes, **settings)


if __name__ == '__main__':
    app = Application()
    version, addr, port = options.version, options.addr, options.port
    app.listen(port, addr)
    logging.info('minirobots api v%s started at %s:%s' % (version, addr, port))
    tornado.ioloop.IOLoop.current().start()
