#!/bin/bash

if [ ! -d env ]
then
    echo "---------------------------------------------"
    echo "Environment not found. Creating..."
    echo "---------------------------------------------"
    python3 -m venv env \
      && source env/bin/activate \
      && pip install --upgrade pip \
      && pip install wheel \
      && pip install --no-cache-dir -r requirements.txt
    deactivate
fi
