#!/bin/bash

bin/create_env.sh

source env/bin/activate
sphinx-build -b html doc doc/_build
deactivate
