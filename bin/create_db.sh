#!/bin/bash

bin/create_env.sh

if [ ! -f api/iot.db ]
then
    echo "---------------------------------------------"
    echo "Api DB not found. Creating..."
    echo "---------------------------------------------"
    source env/bin/activate
    python create_db.py
    deactivate
fi
