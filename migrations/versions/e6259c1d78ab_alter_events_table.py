"""alter events table

Revision ID: e6259c1d78ab
Revises: 7ad477d2bdc5
Create Date: 2018-09-08 19:10:53.069961

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e6259c1d78ab'
down_revision = '7ad477d2bdc5'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'events',
        sa.Column('version', sa.String(length=20), nullable=False)
    )
    op.add_column(
        'events',
        sa.Column('cur_version', sa.String(length=20), nullable=False)
    )


def downgrade():
    op.drop_column('events', 'version')
    op.drop_column('events', 'cur_version')
