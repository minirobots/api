"""initial migration

Revision ID: 7ad477d2bdc5
Revises: 
Create Date: 2018-06-10 17:33:50.480338

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ad477d2bdc5'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('users',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('email', sa.String(length=128), nullable=False),
        sa.Column('name', sa.String(length=128), nullable=False),
        sa.Column('google_id', sa.String(length=128), nullable=True),
        sa.Column('facebook_id', sa.String(length=128), nullable=True),
        sa.Column('picture', sa.String(length=256), nullable=True),
        sa.Column('created', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.Column('updated', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('email'),
        sa.UniqueConstraint('facebook_id'),
        sa.UniqueConstraint('google_id')
    )
    op.create_table('programs',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(length=256), nullable=False),
        sa.Column('code', sa.Text(), nullable=False),
        sa.Column('public', sa.Boolean(), nullable=False),
        sa.Column('created', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.Column('updated', sa.DateTime(), nullable=True),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table('robots',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('mac', sa.String(length=17), nullable=False),
        sa.Column('code', sa.String(length=10), nullable=False),
        sa.Column('name', sa.String(length=128), nullable=False),
        sa.Column('created', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.Column('updated', sa.DateTime(), nullable=True),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('code'),
        sa.UniqueConstraint('mac')
    )
    op.create_table('events',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('ip', sa.String(length=15), nullable=True),
        sa.Column('created', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.Column('robot_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['robot_id'], ['robots.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('events')
    op.drop_table('robots')
    op.drop_table('programs')
    op.drop_table('users')
