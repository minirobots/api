"""alter robots table

Revision ID: 8975ee5a7f44
Revises: e6259c1d78ab
Create Date: 2019-05-26 20:42:06.433951

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8975ee5a7f44'
down_revision = 'e6259c1d78ab'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('robots', sa.Column(
        'pen_up',
        sa.Integer,
        nullable=False,
        server_default="-1"
    ))
    op.add_column('robots', sa.Column(
        'pen_down',
        sa.Integer,
        nullable=False,
        server_default="-1"
    ))
    op.add_column('robots', sa.Column(
        'steps_per_degree',
        sa.Float,
        nullable=False,
        server_default="-1"
    ))
    op.add_column('robots', sa.Column(
        'steps_per_mm',
        sa.Float,
        nullable=False,
        server_default="-1"
    ))


def downgrade():
    op.drop_column('robots', 'pen_up')
    op.drop_column('robots', 'pen_down')
    op.drop_column('robots', 'steps_per_degree')
    op.drop_column('robots', 'steps_per_mm')
