FROM alpine:latest

MAINTAINER Leo Vidarte "lvidarte@gmail.com"

RUN apk add --update ca-certificates python3 && rm -rf /var/cache/apk/*

COPY . /app

WORKDIR /app

RUN pip3 install --upgrade pip \
 && pip3 install --no-cache-dir wheel \
 && pip3 install --no-cache-dir -r requirements.txt

CMD ["python3", "server.py"]
