"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from minirobots import options
from minirobots.models import Base, Robot, User, Alembic

from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, drop_database, database_exists
from sqlalchemy.orm import sessionmaker

from contextlib import contextmanager


engine = create_engine(options.db_path, isolation_level="READ UNCOMMITTED")

Session = sessionmaker(bind=engine)


@contextmanager
def make_session():
    """Provide a transactional scope around a series of operations."""
    session = None
    try:
        session = Session()
        yield session
    except Exception:
        if session:
            session.rollback()
        raise
    else:
        session.commit()
    finally:
        if session:
            session.close()

def drop_db():
    if database_exists(options.db_path):
        print("Drop database")
        drop_database(options.db_path)

def create_db():
    print("Create database")
    create_database(options.db_path)
    Base.metadata.create_all(engine)

def populate_db():
    print("Populate database")
    with make_session() as session:
        user = User(name="test", email="test@minirobots.com.ar")
        session.add(user)
        robot = Robot(mac="00:00:00:00:00:00", code="0000-aaaaa", name="myrobot", user=user)
        session.add(robot)
        alembic = Alembic(version_num=options.alembic_version_num)
        session.add(alembic)

