"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from tornado.gen import coroutine
from tornado.auth import GoogleOAuth2Mixin, FacebookGraphMixin

from minirobots import options
from minirobots import jwt
from minirobots.handlers import BaseHandler
from minirobots.models import User
from minirobots.hashids import id_encode


class GoogleAuthHandler(BaseHandler, GoogleOAuth2Mixin):

    @coroutine
    def get(self):
        if self.get_argument('code', False):
            redirect_uri = self.get_argument(
                'redirect_uri',
                options.google_redirect_uri)
            try:
                access_dict = yield self.get_authenticated_user(
                    redirect_uri=redirect_uri,
                    code=self.get_argument('code'))
            except Exception as e:
                self.set_status(400)
                self.write({'error': str(e)})
                return
            user_dict = yield self.oauth2_request(
                'https://www.googleapis.com/oauth2/v1/userinfo',
                access_token=access_dict['access_token'])
            self._on_auth(user_dict)
        else:
            yield self.authorize_redirect(
                redirect_uri=options.google_redirect_uri,
                client_id=options.google_client_id,
                scope=['profile', 'email'],
                response_type='code',
                extra_params={'approval_prompt': 'auto'})


    def _on_auth(self, user_dict):
        user = self._get_user(user_dict)
        self.write({
            'token': jwt.encode({
                'sub': id_encode(user.id),
            }),
            'user': {
                'id'     : id_encode(user.id),
                'created': options.date_format.format(user.created),
                'name'   : user.name,
                'email'  : user.email,
                'picture': user.picture,
            },
        })


    def _get_user(self, user_dict):
        user = None

        res = self.session.query(User).filter(User.email == user_dict['email'])
        if res.count():
            user = res.first()
            update = False
            if user.google_id != user_dict['id']:
                user.google_id = user_dict['id']
                update = True
            if user.name != user_dict['name']:
                user.name = user_dict['name']
                update = True
            if user.picture != user_dict['picture']:
                user.picture = user_dict['picture']
                update = True
            if update:
                self.session.add(user)
                self.session.commit()

        if user is None:
            user = User(
                email=user_dict['email'],
                name=user_dict['name'],
                google_id=user_dict['id'],
                picture=user_dict['picture']
            )
            self.session.add(user)
            self.session.commit()

        return user


class FacebookAuthHandler(BaseHandler, FacebookGraphMixin):

    @coroutine
    def get(self):
        if self.get_argument('code', False):
            redirect_uri = self.get_argument(
                    'redirect_uri',
                    options.facebook_redirect_uri)
            try:
                user_dict = yield self.get_authenticated_user(
                    redirect_uri=redirect_uri,
                    client_id=options.facebook_client_id,
                    client_secret=options.facebook_client_secret,
                    code=self.get_argument("code"))
            except Exception as e:
                self.set_status(400)
                self.write({'error': str(e)})
                return
            email_dict = yield self.oauth2_request(
                'https://graph.facebook.com/v2.12/me',
                fields='email',
                access_token=user_dict['access_token'])
            self._on_auth(user_dict, email_dict)
        else:
            yield self.authorize_redirect(
                redirect_uri=options.facebook_redirect_uri,
                client_id=options.facebook_client_id,
                extra_params={"scope": "public_profile,email,user_friends"})


    def _on_auth(self, user_dict, email_dict):
        if 'email' not in user_dict or user_dict['email'] == '':
            if 'email' in email_dict and email_dict['email'] != '':
                user_dict['email'] = email_dict['email']
            else:
                user_dict['email'] = ''

        user = self._get_user(user_dict)

        self.write({
            'token': jwt.encode({
                'sub': id_encode(user.id),
            }),
            'user': {
                'id'     : id_encode(user.id),
                'created': options.date_format.format(user.created),
                'name'   : user.name,
                'email'  : user.email,
                'picture': user.picture,
            },
        })


    def _get_user(self, user_dict):
        user = None

        if user_dict['email']:
            # Find by email
            res = self.session.query(User).filter(User.email == user_dict['email'])
            if res.count():
                user = res.first()
                update = False
                if user.facebook_id != user_dict['id']:
                    user.facebook_id = user_dict['id']
                    update = True
                if user.name != user_dict['name']:
                    user.name = user_dict['name']
                    update = True
                if user.picture != user_dict['picture']['data']['url']:
                    user.picture = user_dict['picture']['data']['url']
                    update = True
                if update:
                    self.session.add(user)
                    self.session.commit()
        else:
            # Find by facebook_id
            res = self.session.query(User).filter(User.facebook_id == user_dict['id'])
            if res.count():
                user = res.first()
                update = False
                if user.name != user_dict['name']:
                    user.name = user_dict['name']
                    update = True
                if user.picture != user_dict['picture']['data']['url']:
                    user.picture = user_dict['picture']['data']['url']
                    update = True
                if update:
                    self.session.add(user)
                    self.session.commit()

        if user is None:
            user = User(
                email=user_dict['email'],
                name=user_dict['name'],
                facebook_id=user_dict['id'],
                picture=user_dict['picture']['data']['url'])
            self.session.add(user)
            self.session.commit()

        return user


