"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import json

from minirobots import options
from minirobots.auth import jwtauth
from minirobots.models import Robot
from minirobots.hashids import id_encode, hashid_decode
from minirobots.handlers import BaseHandler


@jwtauth
class RobotsHandler(BaseHandler):

    def post(self):
        if not self.request.body:
            return self.error(400, "missing data")

        data = json.loads(self.request.body.decode('utf8'))
        if 'code' not in data:
            return self.error(400, "missing `code' field")
        if 'name' not in data:
            return self.error(400, "missing `name' field")

        robot = self.session.query(Robot).filter(Robot.code == data['code']).first()
        if robot is None:
            return self.error(404, "robot not found")

        if robot.user_id is not None:
            return self.error(400, "robot already linked")

        robot.user_id = self.user_id
        robot.name = data['name']

        self.session.add(robot)
        self.session.commit()

        return self.write({
            'message': 'robot linked',
            'id'     : id_encode(robot.id),
        })


@jwtauth
class RobotHandler(BaseHandler):

    def get(self, hashid):
        robot_id = hashid_decode(hashid)
        if robot_id is None:
            return self.error(400, "invalid robot `id'")

        robot = self.session.query(Robot).get(robot_id)
        if not robot or robot.user_id != self.user_id:
            return self.error(404, "Robot not found")

        last_event = None
        if len(robot.events):
            last_event = robot.events[0]
            last_event = {
                'created': options.date_format.format(last_event.created),
                'ip'     : last_event.ip,
            }

        return self.write({
            'robot'  : {
                'id'        : id_encode(robot.id),
                'code'      : robot.code,
                'name'      : robot.name,
                'mac'       : robot.mac,
                'created'   : options.date_format.format(robot.created),
                'updated'   : options.date_format.format(robot.updated),
                'last_event': last_event,
            }
        })


@jwtauth
class RobotEventsHandler(BaseHandler):

    def get(self, hashid):
        robot_id = hashid_decode(hashid)
        if robot_id is None:
            return self.error(400, "invalid robot `id'")

        robot = self.session.query(Robot).get(robot_id)
        if not robot or robot.user_id != self.user_id:
            return self.error(404, "Robot not found")

        events = []
        for e in robot.events:
            events.append({
                'created': options.date_format.format(e.created),
                'ip'     : e.ip,
            })

        return self.write({
            'events' : events,
        })


class RobotIPHandler(BaseHandler):

    def get(self, code):
        robot = self.session.query(Robot).filter(Robot.code == code).first()
        if robot is None:
            return self.error(404, "Robot not found")

        ip = None
        created = None
        if len(robot.events):
            last_event = robot.events[0]
            ip = last_event.ip
            created = options.date_format.format(last_event.created)

        return self.write({
            'robot'  : {
                'ip'     : ip,
                'created': created,
            }
        })
