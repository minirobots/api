"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import json
import tornado.web

from minirobots.auth import jwtauth
from minirobots.models import Robot, Event
from minirobots.hashids import id_encode, hashid_decode
from minirobots.handlers import BaseHandler


class EventsHandler(BaseHandler):

    def post(self):
        if not self.request.body:
            return self.error(400, "missing data")

        data = json.loads(self.request.body.decode('utf8'))
        if 'mac' not in data:
            return self.error(400, "missing `mac' field")
        if 'ip' not in data:
            return self.error(400, "missing `ip' field")

        with self.make_session() as session:
            robot = session.query(Robot).filter(Robot.mac == data['mac']).first()
            if robot is None:
                return self.error(404, "robot not found")

            event = Event(robot=robot, ip=data['ip'], version="", cur_version="")
            session.add(event)
            session.commit()
            event_hashid = id_encode(event.id)

            self.set_status(201)
            return self.write({
                'message': 'event created',
                'id'     : event_hashid,
            })

