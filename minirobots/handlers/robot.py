"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import os
import re

from minirobots import options
from minirobots.models import Robot, Event
from minirobots.handlers import BaseHandler


class RobotHandler(BaseHandler):

    def _get_mac(self):
        mac = self.get_argument('mac', '')
        if re.match("^([0-9a-f]{2}:){5}([0-9a-f]{2})$", mac.lower()):
            return mac.lower()
        else:
            return None

    def _get_ip(self):
        ip = self.get_argument('ip', '')
        if re.match("^([0-9]{1,3}\.){3}[0-9]{1,3}$", ip):
            return ip
        else:
            return None

    def _get_version(self):
        version = self.get_argument('version', '')
        if re.match("^([0-9]{1,3}\.){2}[0-9]{1,3}(-[a-zA-Z]*)?$", version):
            return version
        else:
            return None

    def _get_cur_version(self):
        files = os.listdir(options.firmware_dir)
        versions = [f[:-4].split('-') for f in files]
        versions.sort(key=lambda v: list(map(int, v[0].split('.'))))
        return '-'.join(versions[-1])

    def _check_update(self, version, cur_version):
        version = list(map(int, version.split('.')))
        cur_version = list(map(int, cur_version.split('.')))
        return cur_version[0] > version[0] or \
               cur_version[1] > version[1] or \
               cur_version[2] > version[2]


class RobotConfigHandler(RobotHandler):

    def get(self):
        mac = self._get_mac()
        if not mac:
            return self.error(400, "missing `mac' field")

        ip = self._get_ip()
        if ip is None:
            return self.error(400, "missing `ip' field")

        version = self._get_version()
        if version is None:
            return self.error(400, "missing `version' field")

        with self.make_session() as session:
            robot = session.query(Robot).filter(Robot.mac == mac).first()
            if robot is None:
                self.set_status(400)
                self.finish()
            else:
                cur_version = self._get_cur_version()
                event = Event(robot=robot, ip=ip, version=version, cur_version=cur_version)
                session.add(event)
                session.commit()

                return self.write({
                    'firmware_update' : self._check_update(version, cur_version),
                    'pen_up'          : robot.pen_up,
                    'pen_down'        : robot.pen_down,
                    'steps_per_degree': robot.steps_per_degree,
                    'steps_per_mm'    : robot.steps_per_mm,
                })


class RobotFirmwareHandler(RobotHandler):

    def get(self):
        mac = self._get_mac()
        if not mac:
            return self.error(400, "missing `mac' field")

        version = self._get_version()
        if version is None:
            return self.error(400, "missing `version' field")

        with self.make_session() as session:
            robot = session.query(Robot).filter(Robot.mac == mac).first()
            if robot is None:
                self.set_status(400)
                self.finish()
            else:
                cur_version = self._get_cur_version()
                if self._check_update(version, cur_version):
                    filename = cur_version + '.bin'
                    filepath = os.path.join(options.firmware_dir, filename)
                    self.set_header('Content-Type', 'application/octet-stream')
                    self.set_header('Content-Disposition', 'attachment; filename=' + filename)
                    self.set_header('Content-Length', os.path.getsize(filepath))
                    buf_size = 4096
                    with open(filepath, 'rb') as f:
                        while True:
                            data = f.read(buf_size)
                            if not data:
                                break
                            self.write(data)
                    self.finish()
                else:
                    self.set_status(304) # Not Modified
                    self.finish()
