"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import json
import tornado.web

from minirobots import options
from minirobots.auth import jwtauth
from minirobots.models import User, Program
from minirobots.hashids import id_encode, hashid_decode
from minirobots.handlers import BaseHandler


@jwtauth
class UsersHandler(BaseHandler):

    def get(self):
        with self.make_session() as session:
            user = session.query(User).get(self.user_id)
            robots = []
            for robot in user.robots:
                robot_doc = {
                    'id'   : id_encode(robot.id),
                    'code': robot.code,
                    'name': robot.name,
                    'last_event': None,
                }
                if len(robot.events):
                    last_event = robot.events[0]
                    robot_doc['last_event'] = {
                        'created': options.date_format.format(last_event.created),
                        'ip'     : last_event.ip,
                    }
                robots.append(robot_doc)

            return self.write({
                'user': {
                    'id'     : id_encode(user.id),
                    'created': options.date_format.format(user.created),
                    'name'   : user.name,
                    'email'  : user.email,
                    'picture': user.picture,
                    'robots' : robots,
                }
            })


@jwtauth
class UserHandler(BaseHandler):

    def get(self, hashid):
        user_id = hashid_decode(hashid)
        if user_id is None:
            return self.error(400, "invalid user `id'")

        with self.make_session() as session:
            user = session.query(User).get(user_id)
            if not user:
                return self.error(404, "User not found")

            return self.write({
                'user': {
                    'id'     : hashid,
                    'created': options.date_format.format(user.created),
                    'name'   : user.name,
                    'email'  : user.email,
                    'picture': user.picture,
                }
            })


@jwtauth
class ProgramsHandler(BaseHandler):

    def post(self):
        if not self.request.body:
            return self.error(400, "missing data")

        data = json.loads(self.request.body.decode('utf8'))
        if 'name' not in data:
            return self.error(400, "missing `name' field")
        if 'code' not in data:
            return self.error(400, "missing `code' field")

        with self.make_session() as session:
            program = Program(
                name    = data['name'],
                code    = data['code'],
                public  = bool(data['public']) if 'public' in data else False,
                user_id = self.user_id,
            )

            session.add(program)
            session.commit()

            self.set_status(201)
            return self.write({
                'message': 'program created',
                'id'     : id_encode(program.id),
            })


    def get(self):
        with self.make_session() as session:
            user = session.query(User).get(self.user_id)
            programs = []
            for program in user.programs:
                programs.append({
                    'id':      id_encode(program.id),
                    'name':    program.name,
                    'code':    program.code,
                    'public':  program.public,
                    'created': options.date_format.format(program.created),
                    'updated': options.date_format.format(program.updated) if program.updated else None,
                })
            return self.write({'programs': programs})


@jwtauth
class ProgramHandler(BaseHandler):

    def get(self, hashid):
        program_id = hashid_decode(hashid)
        if program_id is None:
            return self.error(400, "invalid program `id'")

        with self.make_session() as session:
            program = session.query(Program)\
                             .filter(Program.id==program_id)\
                             .filter(Program.user_id==self.user_id)
            if not program:
                return self.error(404, "Program not found")

            return self.write({
                'id':      id_encode(program.id),
                'name':    program.name,
                'code':    program.code,
                'public':  program.public,
                'created': options.date_format.format(program.created),
                'updated': options.date_format.format(program.updated) if program.updated else None,
            })


    def put(self, hashid):
        program_id = hashid_decode(hashid)
        if program_id is None:
            return self.error(400, "invalid program `id'")

        data = json.loads(self.request.body.decode('utf8'))

        doc = {}
        if 'name' in data:
            doc['name'] = data['name']
        if 'code' in data:
            doc['code'] = data['code']
        if 'public' in data:
            doc['public'] = bool(data['public'])
        if not doc:
            return self.error(400, "missing fields")

        try:
            with self.make_session() as session:
                session.query(Program)\
                       .filter(Program.id==program_id)\
                       .filter(Program.user_id==self.user_id)\
                       .update(doc)
        except Exception as e:
            return self.error(400, e.message())

        return self.write({
            'message': 'program updated',
            'id'     : hashid,
        })


    def delete(self, hashid):
        program_id = hashid_decode(hashid)
        if program_id is None:
            return self.error(400, "invalid program `id'")

        try:
            with self.make_session() as session:
                session.query(Program)\
                       .filter(Program.id==program_id)\
                       .filter(Program.user_id==self.user_id)\
                       .delete()
        except Exception as e:
            return self.error(400, e.message())

        return self.write({
            'message': 'program deleted',
            'id'     : hashid,
        })

