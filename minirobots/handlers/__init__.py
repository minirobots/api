"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from tornado.web import RequestHandler

from datetime import datetime
import tornado.web

from minirobots import options
from minirobots.auth import jwtauth
from minirobots.sqlalchemy import SessionMixin


class BaseHandler(RequestHandler, SessionMixin):

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    def error(self, status_code, error_message):
        self.set_status(status_code)
        return self.write({"error": error_message})


class HomeHandler(BaseHandler):

    def get(self):
        return self.write({
            'message' : "Welcome to Minirobots API",
            'datetime': "{:%c}".format(datetime.now()),
            'version' : options.version,
        })

