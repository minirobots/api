"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import tornado.web

from minirobots import options


class DocHandler(tornado.web.StaticFileHandler):

    def parse_url_path(self, url_path):
        if not url_path:
            self.redirect('/doc/index.html')
        if url_path.endswith('/'):
            url_path += 'index.html'
        return url_path

