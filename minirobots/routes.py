"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from minirobots import options
from minirobots.handlers import HomeHandler
from minirobots.handlers import auth, users, robots, events
from minirobots.handlers import robot


routes = [
    (r'/',     HomeHandler),

    (r'/auth/google',   auth.GoogleAuthHandler),
    (r'/auth/facebook', auth.FacebookAuthHandler),

    (r'/users/programs',       users.ProgramsHandler),
    (r'/users/programs/(\w+)', users.ProgramHandler),

    (r'/users',       users.UsersHandler),
    (r'/users/(\w+)', users.UserHandler),

    (r'/robots/events', events.EventsHandler),

    (r'/robots',              robots.RobotsHandler),
    (r'/robots/(\w+)',        robots.RobotHandler),
    (r'/robots/(\w+)/ip',     robots.RobotIPHandler),
    (r'/robots/(\w+)/events', robots.RobotEventsHandler),

    # From robot
    (r'/robot/config',   robot.RobotConfigHandler),
    (r'/robot/firmware', robot.RobotFirmwareHandler),
]

if options.doc_enabled:
    import os.path
    from minirobots.handlers import doc
    doc_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), '../doc/_build'))
    routes.append((r'/doc/?(.*)', doc.DocHandler, {'path': doc_dir}))

