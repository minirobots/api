"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import jwt

from minirobots import options
from minirobots.models import User
from minirobots.hashids import hashid_decode


secret_key = options.jwt_secret
jwt_options = {
    'verify_signature': True,
    'verify_exp'      : True,
    'verify_nbf'      : False,
    'verify_iat'      : True,
    'verify_aud'      : False
}


def jwtauth(handler_class):
    """ Handle Tornado JWT Auth Decorator """

    def wrap_execute(handler_execute):

        def require_auth(handler, kwargs):

            authorization = handler.request.headers.get('Authorization', None)
            access_token = handler.request.arguments.get('access_token', None)

            def send_error(status, error):
                handler._transforms = []
                handler.set_status(status)
                handler.write({'error': error})
                handler.finish()

            if authorization is None and access_token is None:
                return send_error(401, "Missing authorization")

            if authorization:
                parts = authorization.split()
                if len(parts) != 2 or parts[0].lower() != 'bearer':
                    return send_error(401, "Invalid header authorization")
                token = parts[1]
            elif access_token:
                token = access_token[0].decode('ascii')

            # Decode JWT
            try:
                handler.payload = jwt.decode(
                    token,
                    secret_key,
                    options=jwt_options)
                handler.user_id = hashid_decode(handler.payload['sub'])
            except Exception as e:
                return send_error(401, str(e))

            return True


        def _execute(self, transforms, *args, **kwargs):
            try:
                require_auth(self, kwargs)
            except Exception:
                return False
            return handler_execute(self, transforms, *args, **kwargs)
        return _execute


    handler_class._execute = wrap_execute(handler_class._execute)
    return handler_class
