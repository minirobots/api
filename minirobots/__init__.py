"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import os.path
import logging
import yaml


class Options:
    pass


options = Options()

options.BASEDIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), '..'
))

#@fixme
options.log_level = logging.INFO

with open('VERSION') as f:
    options.version = f.read().replace('\n', '')

options.debug   = True
options.addr    = '0.0.0.0'
options.port    = 5000

options.date_format = '{:%Y-%m-%d %H:%M:%S}'

#
# https://developers.google.com/identity/sign-in/web/devconsole-project
#
options.google_redirect_uri  = 'http://localhost:5000/auth'
options.google_client_id     = ''
options.google_client_secret = ''

options.jwt_secret = 'FZPgREugJdjANDj0GQQgKW8073Y5eQMG'
options.jwt_ttl = 60 * 60 * 24 # 1 day

options.db_path = 'sqlite:///minirobots.db'

options.hashids_salt = 'wWeJ%qA$!C5O%DDU64f_lTWDwO%MA2qG'

options.firmware_dir = os.path.join(options.BASEDIR, 'firmware')


try:
    filepath = os.path.join(options.BASEDIR, 'config.yaml')
    with open(filepath) as f:
        for k, v in yaml.load(f).items():
            setattr(options, k, v)
except:
    print("config.yaml file not found, using defaults")
    pass

