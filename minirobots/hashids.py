"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from minirobots import options
from hashids import Hashids

hashids = Hashids(salt=options.hashids_salt, min_length=8)

def id_encode(i):
    return hashids.encode(i)

def hashid_decode(s):
    t = hashids.decode(s)
    if len(t):
        return t[0]
    else:
        return None
