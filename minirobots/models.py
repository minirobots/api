"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

from sqlalchemy import (
    Table, Column, ForeignKey,
    Integer, String, Text, DateTime, Boolean, Float
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id          = Column(Integer, primary_key=True)
    email       = Column(String(128), nullable=False, unique=True)
    name        = Column(String(128), nullable=False)
    google_id   = Column(String(128), unique=True)
    facebook_id = Column(String(128), unique=True)
    picture     = Column(String(256))
    created     = Column(DateTime, nullable=False, server_default=func.now())
    updated     = Column(DateTime, onupdate=func.now())

    robots      = relationship("Robot",
                      order_by="desc(Robot.name)",
                      primaryjoin="Robot.user_id==User.id"
                  )

    programs    = relationship("Program",
                    order_by="desc(Program.name)",
                    primaryjoin="Program.user_id==User.id"
                )


class Robot(Base):
    __tablename__ = "robots"

    id       = Column(Integer, primary_key=True)
    mac      = Column(String(17), nullable=False, unique=True)
    code     = Column(String(10), nullable=False, unique=True)
    name     = Column(String(128), nullable=False)
    created  = Column(DateTime, nullable=False, server_default=func.now())
    updated  = Column(DateTime, onupdate=func.now())

    user_id  = Column(Integer, ForeignKey('users.id'), nullable=True)
    user     = relationship(User, primaryjoin='Robot.user_id==User.id')

    pen_up   = Column(Integer, nullable=False, default=-1)
    pen_down = Column(Integer, nullable=False, default=-1)

    steps_per_degree = Column(Float, nullable=False, default=-1)
    steps_per_mm     = Column(Float, nullable=False, default=-1)

    events   = relationship("Event",
                   order_by="desc(Event.created)",
                   primaryjoin="Event.robot_id==Robot.id"
               )


class Event(Base):
    __tablename__ = "events"

    id       = Column(Integer, primary_key=True)
    ip       = Column(String(15))
    version  = Column(String(20))
    created  = Column(DateTime, nullable=False, server_default=func.now())

    cur_version = Column(String(20))

    robot_id = Column(Integer, ForeignKey('robots.id'), nullable=False)
    robot    = relationship(Robot, primaryjoin='Event.robot_id==Robot.id')


class Program(Base):
    __tablename__ = "programs"

    id       = Column(Integer, primary_key=True)
    name     = Column(String(256), nullable=False)
    code     = Column(Text(), nullable=False)
    public   = Column(Boolean(), nullable=False, default=False)
    created  = Column(DateTime, nullable=False, server_default=func.now())
    updated  = Column(DateTime, onupdate=func.now())

    user_id  = Column(Integer, ForeignKey('users.id'), nullable=False)
    user     = relationship(User, primaryjoin='Program.user_id==User.id')


class Alembic(Base):
    __tablename__ = "alembic_version"

    version_num = Column(String(32), nullable=False, primary_key=True)

