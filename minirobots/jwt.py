"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""

import jwt
from datetime import datetime, timedelta

from minirobots import options


def encode(payload):
    payload['exp'] = datetime.utcnow() + timedelta(seconds=options.jwt_ttl)
    token = jwt.encode(
        payload,
        options.jwt_secret,
        algorithm='HS256'
    )
    return token.decode('ascii')


def decode(encoded, verify=True):
    try:
        return jwt.decode(encoded), None
    except jwt.ExpiredSignatureError as e:
        return None, str(e)

