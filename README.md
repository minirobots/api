# Minirobots API

[![pipeline status](https://gitlab.com/minirobots/api/badges/master/pipeline.svg)](https://gitlab.com/minirobots/api/commits/master)


## Docker build and run

    docker-compose [-d] up

## Manually build api

    docker-compose build

## Create db

    bin/create_db.sh

    ----- OR -----

    docker exec -it minirobots-api python3 create_db.py

## Manually run db

    docker run --name minirobots-db --volume data:/var/lib/mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=qwerty -d mariadb:10.3

## Client

    docker run -it --rm --name minirobots-db-client --volume $HOME/.inputrc:/root/.inputrc --link minirobots-db mariadb:10.3 mysql -u root -pqwerty -h minirobots-db minirobots

## Manually run api

    docker run --name minirobots-api --link minirotobs-db -p 5000:5000 minirobots/api

## Documentation build

    sphinx-build -b html doc doc/_build

## Run tests suite

    python -m unittest [-v]

## Create alembic migration

    alembic revision -m 'alter robots table'

## Upgrade DB

    PYTHONPATH=. alembic upgrade head
