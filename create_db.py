"""
Author: Leo Vidarte <https://minirobots.com.ar>

This is free software,
you can redistribute it and/or modify it
under the terms of the GPL version 3
as published by the Free Software Foundation.

"""


if __name__ == '__main__':

    from minirobots.db import drop_db, create_db, populate_db

    drop_db()
    create_db()
    populate_db()

