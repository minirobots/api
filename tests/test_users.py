import json
import logging

from tornado.testing import AsyncHTTPTestCase, gen_test
from tornado.web import Application
from tornado.httpclient import HTTPRequest, HTTPError

from minirobots import options
from minirobots import jwt
from minirobots.handlers import users
from minirobots.hashids import id_encode
from minirobots.sqlalchemy import session_factory


class TestUsers(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()
        logging.getLogger('tornado.access').disabled = True


    def get_token(self):
        return jwt.encode({
            'sub': id_encode(1),
        })


    def get_app(self):
        return Application(
            [ (r"/users", users.UsersHandler), ], 
            session_factory=session_factory(options.db_path)
        )


    @gen_test
    def test_users_not_authorized(self):
        with self.assertRaises(HTTPError) as context:
            response = yield self.http_client.fetch(
                self.get_url('/users')
            )

        response_dict = json.loads(context.exception.response.body.decode('utf8'))

        self.assertEqual(context.exception.response.code, 401)
        self.assertEqual(response_dict, {"error": "Missing authorization"})


    @gen_test
    def test_users_ok(self):
        request = HTTPRequest(
            self.get_url('/users'),
            method="GET",
            headers={'Authorization': "Bearer {}".format(self.get_token())}
        )

        response = yield self.http_client.fetch(request)
        response_dict = json.loads(response.body.decode('utf8'))

        self.assertEqual(response.code, 200)
        self.assertIn('user', response_dict)
