import json
import logging

from tornado.testing import AsyncHTTPTestCase, gen_test
from tornado.web import Application
from tornado.httpclient import HTTPRequest, HTTPError

from minirobots import options
from minirobots import jwt
from minirobots.handlers import users
from minirobots.hashids import id_encode
from minirobots.sqlalchemy import session_factory


class TestAuth(AsyncHTTPTestCase):

    def setUp(self):
        super().setUp()
        logging.getLogger('tornado.access').disabled = True


    def get_token(self, user_id=1):
        return jwt.encode({
            'sub': id_encode(user_id),
        })


    def get_app(self):
        return Application(
            [ (r"/users", users.UsersHandler), ], 
            session_factory=session_factory(options.db_path)
        )


    @gen_test
    def test_auth_missing_authorization(self):
        with self.assertRaises(HTTPError) as context:
            response = yield self.http_client.fetch(
                self.get_url('/users'))

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertEqual(res_dict, {"error": "Missing authorization"})


    @gen_test
    def test_auth_invalid_header_authorization(self):
        with self.assertRaises(HTTPError) as context:
            request = HTTPRequest(
                self.get_url('/users'),
                method="GET",
                headers={'Authorization': self.get_token()})
            response = yield self.http_client.fetch(request)

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertEqual(res_dict, {"error": "Invalid header authorization"})


    @gen_test
    def test_auth_invalid_jwt_by_query(self):
        with self.assertRaises(HTTPError) as context:
            request = HTTPRequest(
                self.get_url('/users?access_token=xx.yy.zz'))
            response = yield self.http_client.fetch(request)

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertIn("Invalid header string", res_dict['error'])


    @gen_test
    def test_auth_invalid_jwt_by_header(self):
        with self.assertRaises(HTTPError) as context:
            request = HTTPRequest(
                self.get_url('/users'),
                method="GET",
                headers={'Authorization': 'Bearer xx.yy.zz'})
            response = yield self.http_client.fetch(request)

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertIn("Invalid header string", res_dict['error'])


    @gen_test
    def _test_auth_user_not_found_by_query(self):
        with self.assertRaises(HTTPError) as context:
            request = HTTPRequest(
                self.get_url('/users?access_token=' + self.get_token(user_id=0)))
            response = yield self.http_client.fetch(request)

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertEqual(res_dict, {"error": "User not found"})


    @gen_test
    def _test_auth_user_not_found_by_header(self):
        with self.assertRaises(HTTPError) as context:
            request = HTTPRequest(
                self.get_url('/users'),
                method="GET",
                headers={'Authorization': 'Bearer ' + self.get_token(user_id=0)})
            response = yield self.http_client.fetch(request)

        res = context.exception.response
        res_dict = json.loads(res.body.decode('utf8'))

        self.assertEqual(res.code, 401)
        self.assertEqual(res_dict, {"error": "User not found"})

