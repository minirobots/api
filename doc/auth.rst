Auth
====

Login with Google code
----------------------

**GET /auth/google?code=<code>&redirect_uri=<uri>**

Checks the code against Google and generates a JWT.

.. code-block:: text

    curl https://api.minirobots.com.ar/auth/google?code=<code>&redirect_uri=<uri>

.. code-block:: json

    {
        "token": "...",
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://lh6.googleusercontent.com/.../photo.jpg"
        }
    }


Login without Google code
-------------------------

**GET /auth/google**

Redirects to Google login screen, validates de user and generates a JWT.

.. code-block:: text

    https://api.minirobots.com.ar/auth/google

.. code-block:: json

    {
        "token": "...",
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://lh6.googleusercontent.com/.../photo.jpg"
        }
    }


Login with Facebook code
------------------------

**GET /auth/facebook?code=<code>&redirect_uri=<uri>**

Checks the code against Facebook and generates a JWT.

.. code-block:: text

    curl https://api.minirobots.com.ar/auth/facebook?code=<code>&redirect_uri=<uri>

.. code-block:: json

    {
        "token": "...",
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://platform-lookaside.fbsbx.com/platform/profilepic/photo.jpg"
        }
    }


Login without Facebook code
---------------------------

**GET /auth/facebook**

Redirects to Facebook login screen, validates de user and generates a JWT.

.. code-block:: text

    https://api.minirobots.com.ar/auth/facebook

.. code-block:: json

    {
        "token": "...",
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://platform-lookaside.fbsbx.com/platform/profilepic/photo.jpg"
        }
    }

