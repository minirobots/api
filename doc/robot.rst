Robot API
=========

Robot Config
------------

**GET /**

Get the robot current config.

.. code-block:: text

    curl http://<robot-ip>/

.. code-block:: json

    {
        "message": "Welcome to Minirobots Turtle API! :)",
        "robot": {
            "id": "minirobots-1528b0",
            "ip": "192.168.1.115",
            "mac": "2c:3a:e8:15:28:b0",
            "firmware": "0.3.3",
            "config": {
                "pen_up": 0,
                "pen_down": 15,
                "steps_per_degree": 7.7192666667,
                "steps_per_mm": 9.8284755764
            }
        }
    }

Robot Status
------------

**GET /status**

Get the robot status.

.. code-block:: text

    curl http://<robot-ip>/status

.. code-block:: json

    {
        "debug": false,
        "queue": {
            "lock": false,
            "size": 512,
            "left": 512
        }
    }

Robot Program
-------------

**POST /program**

Send program to robot.

.. code-block:: text

    curl http://<robot-ip>/program \
         -H 'Content-Type: application/json' \
         -d '{
             "CMD": [
                ["FD", 100],
                ["RT", 90],
                ["LD", [0, 127, 127, 127]]
             ]
         }'

Response (201)

.. code-block:: text

    {
        "message": "Program added"
    }

Error (400)

.. code-block:: text

    {
        "error": "Invalid JSON format"
    }

Robot Program Document
----------------------

Main JSON document

.. code-block:: text

        {
            "CMD": [
                ...
            ]
        }

Commands

.. code-block:: text

    Forward
        [ "FD", DISTANCE ]

        DISTANCE: (uint16_t) 0-65535 mm

    Backward
        [ "BD", DISTANCE ]

        DISTANCE: (uint16_t) 0-65535 mm

    Right
        [ "RT", ANGLE ]

        ANGLE: (uint16_t) 0-65535 degree

    Left
        [ "LT", ANGLE ]

        ANGLE: (uint16_t) 0-65535 degree

    Led
        [ "LD", [ LED, R_COLOR, G_COLOR, B_COLOR ] ]

        LED: (uint8_t)
            0 = right
            1 = left
            2 = both

        R_COLOR, G_COLOR, B_COLOR: (uint8_t) 0-255

    Pen
        [ "PN", ACTION ]

        ACTION: (uint8_t)
            0 = up   (no drawing when moving)
            1 = down (drawing when moving)

    Tone
        [ "TE", [ TONE, DURATION ] ]

        TONE: (uint16_t) 0-65535
        DURATION: (uint16_t) 0-65535 milliseconds

        See `tones` section for notes reference.

    Sleep
        [ "SP", DURATION ]

        DURATION: (uint16_t) 0-65535 milliseconds

    Queue
        [ "QE", ACTION ]

        ACTION: (string)
            lock
            unlock
            clear
