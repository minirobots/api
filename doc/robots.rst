Robots
======

Register a robot
----------------

**POST /robots**

Links robot to an user and sets the robot name.

.. code-block:: text

    curl https://api.minirobots.com.ar/robots \
         -H 'Authorization: Bearer <token>' \
         --data '{
             "code": "2393d4",
             "name": "Mi robot"
         }'

.. code-block:: json

    {
        "id": "0lJLerNa",
        "message": "robot linked"
    }

Future requests will return 400.

.. code-block:: json

    {
        "error": "robot already linked"
    }

Register IP
-----------

**POST /robots/events**

Register the current robot IP.

.. code-block:: text

    curl https://api.minirobots.com.ar/robots/events \
        -H 'Authorization: Bearer <token>' \
        --data '{
            "mac": "5c:cf:7f:23:93:d4",
            "ip" : "192.168.0.2"
        }'

.. code-block:: json

    {
        "message": "event created",
        "id": "0lJLerNa"
    }

Robot details
-------------

**GET /robots/<id>**

Get the robot details.

.. code-block:: text

    curl https://api.minirobots.com.ar/robots/0lJLerNa \
        -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "robot": {
            "id": "01JLerNa",
            "code": "2393d4",
            "name": "Mi robot",
            "mac": "5C:CF:7F:23:93:D4",
            "created": "2018-02-15 12:52:16",
            "updated": "2018-02-18 19:09:14",
            "last_event": {
                "created": "2018-02-18 19:41:16",
                "ip": "192.168.1.39"
            }
        }
    }

Robot events
------------

**GET /robots/<id>/events**

Get all robot events.

.. code-block:: text

    curl https://api.minirobots.com.ar/robots/0lJLerNa/events \
        -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "events": [
            {
                "created": "2018-02-18 19:41:16",
                "ip": "192.168.1.39"
            },
            {
                "created": "2018-02-18 19:39:47",
                "ip": "192.168.1.39"
            }
        ]
    }

Robot IP
--------

**GET /robots/<code>/ip**

Get last robot ip.

.. code-block:: text

    curl https://api.minirobots.com.ar/robots/2393d4/ip

.. code-block:: json

    {
        "robot": {
            "ip": "192.168.1.39",
            "created": "2018-02-18 19:41:16"
         }
    }
