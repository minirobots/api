Users
=====

Current user
------------

**GET /users**

Gets the user that belongs to token.

.. code-block:: text

    curl https://api.minirobots.com.ar/users \
        -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://lh6.googleusercontent.com/.../photo.jpg",
            "robots": [
                {
                    "id": "0lJLerNa",
                    "code": "1528b4",
                    "name": "Mi robot",
                    "last_event": null
                }
            ]
        }
    }


Specific user
-------------

**GET /users/{id}**

Gets the user identify by ID.

.. code-block:: text

    curl https://api.minirobots.com.ar/users/0lJLerNa \
        -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "user": {
            "id": "0lJLerNa",
            "created": "2018-02-15 13:57:53",
            "name": "Leo Vidarte",
            "email": "lvidarte@gmail.com",
            "picture": "https://lh6.googleusercontent.com/.../photo.jpg"
        }
    }


User Programs
=============

Create program
--------------

**POST /users/programs**

Creates a program for the user that belongs to token.

.. code-block:: text

    curl https://api.minirobots.com.ar/users/programs \
         -H 'Authorization: Bearer <token>' \
         --data '{
             "name": "Mi programa",
             "code": "<?xml...",
             "public": true
         }'

.. code-block:: json

    {
        "message": "program created",
        "id": "RqO5ory3"
    }

Get programs
------------

**GET /users/programs**

Gets all programs for the user that belongs to token.

.. code-block:: text

    curl https://api.minirobots.com.ar/users/programs \
         -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "programs": [
            {
                "id": "RqO5ory3",
                "name": "Mi programa",
                "code": "<?xml...",
                "public": true,
                "created": "2018-06-10 21:51:38",
                "updated": null
            }
        ]
    }

Update program
--------------

**PUT /users/programs/{id}**

Updates a program for the user that belongs to token.

.. code-block:: text

    curl -XPUT https://api.minirobots.com.ar/users/programs/RqO5ory3 \
         -H 'Authorization: Bearer <token>' \
         --data '{
             "name": "Mi programa v2",
         }'

.. code-block:: json

    {
        "message": "program updated",
        "id": "RqO5ory3"
    }

Delete program
--------------

**DELETE /users/programs/{id}**

Deletes a program for the user that belongs to token.

.. code-block:: text

    curl -XDELETE https://api.minirobots.com.ar/users/programs/RqO5ory3 \
         -H 'Authorization: Bearer <token>'

.. code-block:: json

    {
        "message": "program deleted",
        "id": "RqO5ory3"
    }

