Welcome to Minirobots API's documentation!
==========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   auth
   users
   robots
   Robot API <robot>
   Appendix: Tones <tones>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

